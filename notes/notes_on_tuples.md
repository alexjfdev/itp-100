1. Tuples are like lists because they only contain one data item per entry (whereas dictionaries have 2) and each item can be read individually or it can be read as a whole.
2. Tuples cannot be changed once they are created. The benefit to this is that they can be read faster. Tuples exist for times when a list is only created once but is then read many times after inital assignment.
3. tuple=(1,3,"bananna") Tuple assignment is like list assignment. Also like lists tuples can be used to set several variables at once. a,b,c=tuple

4. >import sys	
import time	
print("Tell me about your dog")	
dog=(input("What is your dog's name? "),input("How old is your dog? "),input("What breed is your dog? "))	
name,age,breed=dog	
print("So your dog's name is "+name)	
time.sleep(1)	
sys.stdout.write("\033[F")	
sys.stdout.write("\033[K")	
print("So your dog's name is "+name+", it is "+age+" years old")	
time.sleep(1)	
sys.stdout.write("\033[F")	
sys.stdout.write("\033[K")	
print("So your dog's name is "+name+", it is "+age+" years old, and it's breed is "+breed)	
time.sleep(5)	

5. List comprehension allows you to easily create lists from other lists.
