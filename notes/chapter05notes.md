# Chapter 5 notes
1. Markdown is used to make nice looking text files easily. People typically use it when working with Git for readmes.
2. Functions are a way of cutting down on the size and complexity of code by making it so that a section of code that you use often only needs to be used once. *Doctor Chuck* is saying that functions are storing code to be reused multiple times.
3. Functions are created with `def "function name"("variables to pass into function"):`. In order for python to recognize code in a function it must be indented after the function is declared otherwise python will think it's something else and your code may not work.
```
def hello(x):
  for y in range(x):
      print("hello")
```
the full code can be found in the root folder of itp-100 and is called hello.py

4. He calls it extended python.
5. Python has all sorts of extended functions. Like *Doctor Chuck* said print is an example of an extended function but so are float(), input(), and int() just to name a few.
