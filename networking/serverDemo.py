from socket import *
port=int(input("What port do you want to use:"))
def create_server():
    serversock = socket(AF_INET, SOCK_STREAM)
    data = 'HTTP/1.1 200 OK\r\n'
    data += 'Context-Type: text/html; charset=utf-8\r\n\r\n'
    data += open("test.html").read()
    try:
        serversock.bind(('0.0.0.0', port))
        serversock.listen(5)

        while True:
            (clientsock, address) = serversock.accept()

            recved = clientsock.recv(5000).decode()
            pieces = recved.split('\n')
            if (len(pieces) > 0):
                print(pieces[0])

            clientsock.sendall(data.encode())
            clientsock.shutdown(SHUT_WR)

    except KeyboardInterrupt:
        print('\nShutting down...\n');
    except Exception as exc:
        print('Error:\n', exc)

    serversock.close()

print('Access http://0.0.0.0:'+str(port))
create_server()
