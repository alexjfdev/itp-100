import socket

host = input("What server:")
port = int(input("What port:"))              # The same port as used by the server
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((host, port))
while True:
    data = s.recv(1024)
    print('Received', repr(data))
