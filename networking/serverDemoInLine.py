from socket import *
port=int(input("What port do you want to use:"))
def create_server():
    serversock = socket(AF_INET, SOCK_STREAM)
    try:
        serversock.bind(('0.0.0.0', port))
        serversock.listen(5)

        while True:
            (clientsock, address) = serversock.accept()

            recved = clientsock.recv(5000).decode()
            pieces = recved.split('\n')
            if (len(pieces) > 0):
                print(pieces[0])

            data = 'HTTP/1.1 200 OK\r\n'
            data += 'Context-Type: text/html; charset=utf-8\r\n\r\n'
            #data += '<!DOCTYPE html>\n<html lang="en">\n<head>\n<meta charset="utf-8">\n<title>Alex\'s Web Server</title>\n</head>\n<body>\n<h1>Hello, from Alex\'s Web Server!</h1>\n</body></html>\r\n\r\n'
            data += '<DOCTYPE html>\n<head>\n<meta http-equiv = "refresh" content = "0; url = <test.html>" />\n</head>'

            clientsock.sendall(data.encode())
            clientsock.shutdown(SHUT_WR)

    except KeyboardInterrupt:
        print('\nShutting down...\n');
    except Exception as exc:
        print('Error:\n', exc)

    serversock.close()

print('Access http://0.0.0.0:'+str(port))
create_server()
