import socket

host = socket.gethostname()
port = int(input("What port:"))              # The same port as used by the server
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((host, port))
message = bytes(input("Message:"), 'utf-8')
s.sendall(message)
data = s.recv(1024)
s.close()
print('Received', repr(data))
