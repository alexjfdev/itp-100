import curses
import time
import threading
import signal
import sys
def signalHandler(sig, frame):
    curses.endwin()
    sys.exit()
signal.signal(signal.SIGINT, signalHandler)
"""
def my_raw_input(stdscr, r, c, prompt_string):
    curses.echo()
    stdscr.addstr(r, c, prompt_string)
    stdscr.refresh()
    input = stdscr.getstr(r + 1, c)
    return input  #       ^^^^  reading input at next line

stdscr=curses.initscr()
curses.start_color()
curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_BLUE)
curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_YELLOW)
inputWin=curses.newwin(2,0,curses.LINES-2,0)
textWin=curses.newwin(curses.LINES-2,0,0,0)
textWin.bkgd(' ', curses.color_pair(1))
inputWin.bkgd(' ', curses.color_pair(2))
textWin.scrollok(True)
textWin.addstr(0,0,"test")
textWin.refresh()
def msgEnter():
    while True:
            msg = my_raw_input(inputWin, 0, 0, "Enter message:")
            textWin.addstr(1,0,msg)
            inputWin.clear()
def printer():
    x=1
    while True:
        time.sleep(1)
        textWin.addstr(curses.LINES-3,0,"test message "+str(x))
        textWin.scroll(1)
        textWin.refresh()
        x=x+1


curses.endwin()
msgOutputThread = threading.Thread(target=msgEnter, args=())
msgOutputThread.start()
msgInputThread = threading.Thread(target=printer, args=())
msgInputThread.start()
"""

"""
stdscr=curses.initscr()
curses.noecho()
curses.cbreak()
topWin=curses.newwin(1,0,0,0)
bottomWin=curses.newwin(2,0,stdscr.getmaxyx()[0]-2,0)
curses.start_color()
curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_GREEN)
curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_YELLOW)
curses.init_pair(3, curses.COLOR_CYAN, curses.COLOR_BLACK)
stdscr.bkgd(' ', curses.color_pair(1))
bottomWin.bkgd(' ', curses.color_pair(2))
topWin.bkgd(' ', curses.color_pair(3))
stdscr.refresh()
bottomWin.refresh()
topWin.refresh()
while True:
    k=str(stdscr.getch())
    if int(k) == 410:
        topWin=curses.newwin(1,0,0,0)
        bottomWin=curses.newwin(2,0,stdscr.getmaxyx()[0]-2,0)
        bottomWin.bkgd(' ', curses.color_pair(2))
        topWin.bkgd(' ', curses.color_pair(3))
        topWin.refresh()
        bottomWin.refresh()
        topWin.addstr(0,0,"410 "+str(stdscr.getmaxyx()[0])+" "+str(stdscr.getmaxyx()[1]))
        topWin.refresh()
    else:
        topWin.clear()
        topWin.addstr(0,0,str(k)+" "+str(stdscr.getmaxyx()[0])+" "+str(stdscr.getmaxyx()[1]))
        topWin.refresh()
"""
stdscr=curses.initscr()
curses.noecho()
curses.cbreak()
curses.start_color()
pairNum=0
stdscr.scrollok(True)
try:
    for b in range(0,255):
        for t in range(0,255):
            pairNum=pairNum+1
            curses.init_pair(pairNum,t,b)
            stdscr.addstr(str(t)+" "+str(b), curses.color_pair(pairNum))
            #time.sleep(1)
            stdscr.refresh()
except:
    stdscr.scroll(1)
stdscr.getch()
curses.endwin()
"""
def main(stdscr):
    curses.start_color()
    curses.use_default_colors()
    for i in range(0, curses.COLORS):
        curses.init_pair(i + 1, i, -1)
    try:
        for i in range(0, 255):
            stdscr.addstr(str(i)+" ", curses.color_pair(i))
    except curses.ERR:
        # End of screen reached
        pass
    stdscr.getch()

curses.wrapper(main)
"""
