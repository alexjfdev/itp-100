def func(lis):
    houses = {"griffindor":0,"ravenclaw":0,"hufflepuff":0,"slytherin":0}
    for lnum in range(len(lis)):
        error= False
        if "Gryffindor" in str(lis[lnum]):
            houses["griffindor"] += 1
        if "Ravenclaw" in str(lis[lnum]):
            houses["ravenclaw"] += 1
        if "Hufflepuff" in str(lis[lnum]):
            houses["hufflepuff"] += 1
        if "Slytherin" in str(lis[lnum]):
            houses["slytherin"] += 1
    for hnum in range(len(houses)):
        if list(houses.values())[hnum] < 7:
            print(list(houses)[hnum]+" does not have enough players they need "+str(7-list(houses.values())[hnum])+" more.")
            error=True
        elif list(houses.values())[hnum] > 7:
            print(list(houses)[hnum]+" has "+str(list(houses.values())[hnum]-7)+" too many players")
            error=True
        else:
            print(list(houses)[hnum]+" has just enough players")
    if error == True:
        print("There was an error in the data. Please renter it and try again.")
    else:
        print("List complete, let’s play quidditch!")
func(open(input("What is the filename?: ")).readlines())
